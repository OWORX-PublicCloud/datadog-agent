#!/bin/sh

if [ "$#" -ne 3 ]; then
    echo "Expecting api key, site and major_version: $0 <api_key> <site> <major_version>"
else

    API_KEY=$1
    SITE=$2
    MAJOR_VERSION=$3

    DD_EXIST=/etc/datadog-agent

    ## Check agent if already installed - We may not want this as it will prevent minor udpates to agents major version
    if [ ! -d $DD_EXIST ]; then

        ## Datadog agent install (cannot use DD one liner install)
        curl -L  https://s3.amazonaws.com/dd-agent/scripts/install_script.sh --output dd_install_script.sh
        chmod +x dd_install_script.sh
        DD_AGENT_MAJOR_VERSION=$MAJOR_VERSION DD_API_KEY=$API_KEY DD_SITE=$SITE bash -c ./dd_install_script.sh

        # ## Instruct the check to collect using mount points instead of volumes.
        cp /etc/datadog-agent/conf.d/disk.d/conf.yaml.default /etc/datadog-agent/conf.d/disk.d/conf.yaml
        sed -i 's/use_mount: false/use_mount: true/g' /etc/datadog-agent/conf.d/disk.d/conf.yaml

        systemctl restart datadog-agent

    else
        echo "Datadog Agent already installed"
    fi
fi