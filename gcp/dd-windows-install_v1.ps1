  <#
    .SYNOPSIS
    This PowerShell script is designed to install the latest Datadog agent seamlessly on Windows

    .PARAMETER APIKey
    The API Key for your datadog installation. Contact support if you don't have one!

    .PARAMETER MajorVersion
    The Major Version of the datadog agent to install. This is required.

    .PARAMETER Site
    Specify DataDog datacenter site location. In most cases this should be set to: 'datadoghq.eu'

    .EXAMPLE
    .\dd-windows-install_v1.ps1 API_KEY SITE MAJOR_VERSION
#>

#Ensure TLS1.2 is used
[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.SecurityProtocolType]::Tls12

# Install and Configure DataDog Agent
$APIKey = $args[0]
$Site = $args[1]
$MajorVersion = $args[2]
$Location = "C:\Windows\Temp"
$MSI = "$Location\ddog.msi"
    If ( Test-path $MSI ) {
        Remove-Item $MSI -Force
    }
Write-Output "Getting versions JSON"
$InstallerJsonUrl ="https://s3.amazonaws.com/ddagent-windows-stable/installers.json"
$Req = Invoke-WebRequest -UseBasicParsing -Uri $InstallerJsonUrl

Write-Output "Calculating latest version"
$VersionsObject = $Req.Content | ConvertFrom-Json
$VersionStrings = $VersionsObject| Get-Member -MemberType NoteProperty | Select Name

$Versions = @()
ForEach ($Version in $VersionStrings){
    If ($Version.Name.StartsWith($MajorVersion)) {
            $Versions += [version] $Version.Name
        }
}
$InstallVersion = $Versions | Sort-Object -Descending | Select -First 1
Write-Output "Collecting Datadog Version $($InstallVersion.ToString())"
$TargetMSI = $VersionsObject.$($InstallVersion.toString()).amd64
Invoke-WebRequest -UseBasicParsing -Uri $TargetMSI -OutFile $MSI
$Expression = "`"$MSI`" APIKEY=`"$APIKey`" SITE=`"$Site`" HOSTNAME=`"$HostNameId`""
Write-Output "Commencing Installation"
Write-Verbose "Installation Command: $Expression"
$Status = Start-Process -FilePath msiexec.exe -ArgumentList /qn,/I,$Expression -Wait

# Post-install config
((Get-Content -path "C:\ProgramData\Datadog\datadog.yaml" -Raw) -replace 'logs_enabled: false','logs_enabled: true') | Set-Content -Path "C:\ProgramData\Datadog\datadog.yaml"

$yamlcontent = "########################
instances:
########################
-
  log_file:
    - System
  type:
    - Error
  event_id:
    - 6008
  source_name:
    - EventLog
  tags:
    - UnexpectedReboot

########################
logs:
########################
  - type: windows_event
    channel_path: Application
    source: Application
    service: eventlog
    sourcecategory: windowsevent

  - type: windows_event
    channel_path: System
    source: System
    service: eventlog
    sourcecategory: windowsevent"

New-Item -Path "C:\ProgramData\Datadog\conf.d" -Name "win32_event_log.d" -ItemType "directory" -Force
New-Item -Path "C:\ProgramData\Datadog\conf.d\win32_event_log.d" `
        -Name "conf.yaml" `
        -ItemType "file" `
        -Value $yamlcontent `
        -Force

Restart-Service datadogagent -Force