#!/bin/bash
# This script is designed to install the latest Datadog agent seamlessly on Linux
# EXAMPLE: ./dd-linux-install.sh API_KEY SITE MAJOR_VERSION
# AWS Cli_V2 installation/update
case $(grep "^ID=" /etc/os-release | cut -d "=" -f 2 | tr [:upper:] [:lower:] | tr -d '"') in
    amzn|rhel|centos)
        #AWS Cli_V2 installation
        yum install -y unzip
        curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"   
        unzip awscliv2.zip
        if [ ! -d /usr/local/aws-cli ]; then
            sudo ./aws/install
            rm -rf ./aws*
        else
            sudo ./aws/install --update
            rm -rf ./aws*
        fi
        ;;
    ubuntu)
        #AWS Cli_V2 installation
        sudo apt-get install unzip
        curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
        unzip awscliv2.zip
        if [ ! -d /usr/local/aws-cli ]; then
            sudo ./aws/install
            rm -rf ./aws*
        else
            sudo ./aws/install --update
            rm -rf ./aws*
        fi
        ;;
esac

# AWS hostname_id generation (concatenation of the Name tag value and the AWS instance ID, example: Web-i-0184dea850461c5fd)
# In case Name tag is unset, hostname_id equals instance's Id. (eg i-0184dea850461c5fd)
instance_id="$(curl -s http://169.254.169.254/latest/meta-data/instance-id)"
# Following line builds a value from Name Tag and Instance Id. In case Name Tag is empty it will contain only Instance Id.
hostname_id="$(echo "$(aws ec2 describe-tags --filters "Name=resource-id,Values=$instance_id" --query 'Tags[?Key==`Name`].Value' --output=text)-$instance_id"|sed '/^-/s/-//')"

# Install and Configure DataDog Agent
if [ "$#" -ne 3 ]; then
    echo "Expecting api key, site and major_version: $0 <api_key> <site> <major_version>"
else

    api_key=$1
    site=$2
    major_version=$3

    DD_EXIST=/etc/datadog-agent  

    ## Check agent if already installed
    if [ ! -d $DD_EXIST ]; then 

        ## Datadog agent install 
        DD_AGENT_MAJOR_VERSION=$major_version DD_HOSTNAME=$hostname_id DD_API_KEY=$api_key DD_SITE=$site \
        bash <(curl -L -N https://s3.amazonaws.com/dd-agent/scripts/install_script.sh) && \

        # ## Instruct the check to collect using mount points instead of volumes. 
        cp /etc/datadog-agent/conf.d/disk.d/conf.yaml.default /etc/datadog-agent/conf.d/disk.d/conf.yaml && \
        sed -i 's/use_mount: false/use_mount: true/g' /etc/datadog-agent/conf.d/disk.d/conf.yaml && \
        systemctl enable datadog-agent && systemctl restart datadog-agent 

    else 
        echo "Datadog Agent already installed" 
    fi 
fi
