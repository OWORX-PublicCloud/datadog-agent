#########################################################
#Uninstall Datadog Agent
#########################################################
Write-Output "Uninstalling Datadog Agent"
(Get-WmiObject -Class Win32_Product | Where-Object{$_.Name -eq "Datadog Agent"}).Uninstall()

#Remove Datadog directories
Write-Output "Removing Datadog directories"
Remove-Item 'C:\Program Files\Datadog' -Recurse
Remove-Item 'C:\ProgramData\Datadog' -Recurse