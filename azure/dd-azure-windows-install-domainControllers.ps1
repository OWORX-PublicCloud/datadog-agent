#########################################################
#Download and Run MSI package for Automated install
#########################################################
Param(
    [parameter(Mandatory=$true)][String]$APIKey,
    [parameter(Mandatory=$true)][String]$Site,
    [parameter(Mandatory=$true)][String]$MajorVersion,
    [parameter(Mandatory=$true)][String]$ServiceAccountName, #In format <domainNetBiosName>\<serviceaccountname>
    [parameter(Mandatory=$true)][String]$ServiceAccountPassword
    )

# Install and Configure DataDog Agent
$Location = "C:\Windows\Temp"
$MSI = "$Location\ddog.msi"
    If ( Test-path $MSI ) {
        Remove-Item $MSI -Force
    }
Write-Output "Getting versions JSON"
$InstallerJsonUrl = "https://s3.amazonaws.com/ddagent-windows-stable/installers.json"
$Req = Invoke-WebRequest -UseBasicParsing -Uri $InstallerJsonUrl

Write-Output "Calculating latest version"
$VersionsObject = $Req.Content | ConvertFrom-Json
$VersionStrings = $VersionsObject| Get-Member -MemberType NoteProperty | Select Name 

$Versions = @()
ForEach ($Version in $VersionStrings){
    If ($Version.Name.StartsWith($MajorVersion)) {
            $Versions += [version] $Version.Name
        }
}
$InstallVersion = $Versions | Sort-Object -Descending | Select -First 1
Write-Output "Collecting Datadog Version $($InstallVersion.ToString())"
$TargetMSI = $VersionsObject.$($InstallVersion.toString()).amd64
Invoke-WebRequest -UseBasicParsing -Uri $TargetMSI -OutFile $MSI
$Expression = "`"$MSI`" APIKEY=`"$APIKey`" SITE=`"$Site`" LOGS_ENABLED=`"true`" DDAGENTUSER_NAME=`"$ServiceAccountName`" DDAGENTUSER_PASSWORD=`"$ServiceAccountPassword`" REBOOT=`"ReallySuppress`""
Write-Output "Commencing Installation"
Write-Verbose "Installation Command: $Expression"
$Status = Start-Process -FilePath msiexec.exe -ArgumentList /qn,/I,$Expression -Wait

#Set Windows eventlog integration
$yamlcontent = "########################
instances:
########################
-
  log_file:
    - System
  type:
    - Error
  event_id:
    - 6008
  source_name:
    - EventLog
  tags:
    - UnexpectedReboot
    - oworxwineventid:6008"

New-Item -Path "C:\ProgramData\Datadog\conf.d" -Name "win32_event_log.d" -ItemType "directory" -Force
New-Item -Path "C:\ProgramData\Datadog\conf.d\win32_event_log.d" `
        -Name "conf.yaml" `
        -ItemType "file" `
        -Value $yamlcontent `
        -Force

#Change cloud provider metadata list from Alibaba,GCP,AWS,Azure to only Azure
#https://github.com/DataDog/datadog-agent/blob/7.20.2/pkg/config/config_template.yaml#L190-L211

$oldCloudProviderMetadataList = @"
# cloud_provider_metadata:
#   - "aws"
#   - "gcp"
#   - "azure"
#   - "alibaba"
"@

$newCloudProviderMetadataList = @"
cloud_provider_metadata:
  - "azure"
"@

(Get-Content -path C:\ProgramData\Datadog\datadog.yaml -Raw) -replace $oldCloudProviderMetadataList,$newCloudProviderMetadataList |
    Set-Content -Path C:\ProgramData\Datadog\datadog.yaml

Restart-Service datadogagent -Force