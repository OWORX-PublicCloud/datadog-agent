#!/bin/sh
case $(grep "^ID=" /etc/os-release | cut -d "=" -f 2 | tr [:upper:] [:lower:] | tr -d '"') in
    rhel|centos)
        #Datadog uninstallation
        yum remove -y datadog-agent
        rm -rf /etc/datadog-agent
        rm -rf /opt/datadog-agent
        userdel -f -r dd-agent
        ;;
    ubuntu|debian)
        #Datadog uninstallation
        apt-get remove --purge datadog-agent -y
        ;;
esac