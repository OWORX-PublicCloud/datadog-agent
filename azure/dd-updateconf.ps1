((Get-Content -path "C:\ProgramData\Datadog\datadog.yaml" -Raw) -replace 'logs_enabled: false','logs_enabled: true') | Set-Content -Path "C:\ProgramData\Datadog\datadog.yaml"

((Get-Content -path "C:\ProgramData\Datadog\datadog.yaml" -Raw) -replace '# logs_enabled: false','logs_enabled: true') | Set-Content -Path "C:\ProgramData\Datadog\datadog.yaml"

((Get-Content -path "C:\ProgramData\Datadog\datadog.yaml" -Raw) -replace '# logs_enabled: true','logs_enabled: true') | Set-Content -Path "C:\ProgramData\Datadog\datadog.yaml"

$yamlcontent = "########################
instances:
########################
-
  log_file:
    - System
  type:
    - Error
  event_id:
    - 6008
  source_name:
    - EventLog
  tags:
    - UnexpectedReboot
    - oworxwineventid:6008"

New-Item -Path "C:\ProgramData\Datadog\conf.d" -Name "win32_event_log.d" -ItemType "directory" -Force
New-Item -Path "C:\ProgramData\Datadog\conf.d\win32_event_log.d" `
        -Name "conf.yaml" `
        -ItemType "file" `
        -Value $yamlcontent `
        -Force

Restart-Service datadogagent -Force