#!/bin/sh
# Install and Configure DataDog Agent
    api_key=$APIKey
    site=$Site
    major_version=$MajorVersion

## Datadog agent install 
DD_AGENT_MAJOR_VERSION=$major_version DD_API_KEY=$api_key DD_SITE=$site bash -c "$(curl -L https://s3.amazonaws.com/dd-agent/scripts/install_script.sh)"

# ## Instruct the check to collect using mount points instead of volumes & adjust cloud provider metadata settings to Azure only
cp /etc/datadog-agent/conf.d/disk.d/conf.yaml.default /etc/datadog-agent/conf.d/disk.d/conf.yaml && \
sed -i 's/use_mount: false/use_mount: true/g' /etc/datadog-agent/conf.d/disk.d/conf.yaml && \
sed -i 's/# cloud_provider_metadata:/cloud_provider_metadata:/g' /etc/datadog-agent/datadog.yaml && \
sed -i 's/#   - "azure"/  - "azure"/g' /etc/datadog-agent/datadog.yaml && \
sed -i '/#   - "aws"/d' /etc/datadog-agent/datadog.yaml && \
sed -i '/#   - "gcp"/d' /etc/datadog-agent/datadog.yaml && \
sed -i '/#   - "alibaba"/d' /etc/datadog-agent/datadog.yaml && \
systemctl enable datadog-agent && systemctl restart datadog-agent